﻿using System;
using System.Collections.Generic;
namespace ExpDataAnalysis
{
    class TwoDimAprox
    {
        public static List<double> LinearAprox(List<double> x, List<double> y, List<double> z)
        {
            double sumx2 = 0;
            double sumxy = 0;
            double sumx = 0;
            double sumy2 = 0;
            double sumy = 0;
            double sumxz = 0;
            double sumyz = 0;
            double sumz = 0;
            int n = x.Count;
            for (int i = 0; i < n; i++)
            {
                sumx2 += Math.Pow(x[i], 2);
                sumxy += x[i] * y[i];
                sumx += x[i];
                sumy2 += Math.Pow(y[i], 2);
                sumy += y[i];
                sumxz += x[i] * z[i];
                sumyz += y[i] * z[i];
                sumz += z[i];
            }
            double d = sumx2 * (sumy2 * n - sumy * sumy) - sumxy * (sumxy * n - sumy * sumx) + sumx * (sumxy * sumy - sumy2 * sumx);
            double d1 = sumxz * (sumy2 * n - sumy * sumy) - sumxy * (sumyz * n - sumy * sumz) + sumx * (sumyz * sumy - sumy2 * sumz);
            double d2 = sumx2 * (sumyz * n - sumz * sumy) - sumxz * (sumxy * n - sumy * sumx) + sumx * (sumxy * sumz - sumyz * sumx);
            double d3 = sumx2 * (sumy2 * sumz - sumyz * sumy) - sumxy * (sumxy * sumz - sumyz * sumx) + sumxz * (sumxy * sumy - sumy2 * sumx);
            double a = d1 / d;
            double b = d2 / d;
            double c = d3 / d;
            return new List<double> { a, b, c };
        }
        public static List<double> QuadraticAprox(List<double> x, List<double> y, List<double> z)
        {
            double sumx4 = 0,
                 sumx3y = 0,
                 sumx2y2 = 0,
                 sumx3 = 0,
                 sumx2y = 0,
                 sumx2 = 0,
                 sumxy3 = 0,
                 sumxy2 = 0,
                 sumxy = 0,
                 sumy4 = 0,
                 sumy3 = 0,
                 sumy2 = 0,
                 sumy = 0,
                 sumx = 0,
                 sumx2z = 0,
                 sumxyz = 0,
                 sumy2z = 0,
                 sumxz = 0,
                 sumyz = 0,
                 sumz = 0;
            int n = x.Count;
            for (int i = 0; i < n; i++)
            {
                sumx4 += Math.Pow(x[i], 4);
                sumx3y += Math.Pow(x[i], 3) * y[i];
                sumx2y2 += Math.Pow(x[i] * y[i], 2);
                sumx3 += Math.Pow(x[i], 3);
                sumx2y += Math.Pow(x[i], 2) * y[i];
                sumx2 += Math.Pow(x[i], 2);
                sumxy3 += x[i] * Math.Pow(y[i], 3);
                sumxy2 += x[i] * Math.Pow(y[i], 2);
                sumxy += x[i] * y[i];
                sumy4 += Math.Pow(y[i], 4);
                sumy3 += Math.Pow(y[i], 3);
                sumy2 += Math.Pow(y[i], 2);
                sumy += y[i];
                sumx += x[i];
                sumx2z += Math.Pow(x[i], 2) * z[i];
                sumxyz += x[i] * y[i] * z[i];
                sumy2z += Math.Pow(y[i], 2) * z[i];
                sumxz += x[i] * z[i];
                sumyz += y[i] * z[i];
                sumz += z[i];
            }
            Matrix A = new Matrix(6, 7);
            A.Set(new double[,] {{sumx4,sumx3y,sumx2y2,sumx3,sumx2y,sumx2,sumx2z },
                               { sumx3y,sumx2y2,sumxy3,sumx2y,sumxy2,sumxy,sumxyz },
                               { sumx2y2,sumxy3,sumy4,sumxy2,sumy3,sumy2,sumy2z },
                               { sumx3,sumx2y,sumxy2,sumx2,sumxy,sumx,sumxz },
                               { sumx2y,sumxy2,sumy3,sumxy,sumy2,sumy2,sumyz },
                               { sumx2,sumxy,sumy2,sumx,sumy,n,sumz } });
            double d0 = A.CreateMatrixWithoutColumn(6).CalculateDeterminant();
            List<double> res = new List<double>();
            for (int i = 0; i < 6; i++)
                res.Add(A.SwapColumns(i, 6).CreateMatrixWithoutColumn(6).CalculateDeterminant() / d0);
            return res;
        }
        public static List<double> DegreeAprox(List<double> x, List<double> y, List<double> z)
        {
            int n = x.Count;
            List<double> lnx = new List<double>();
            List<double> lny = new List<double>();
            List<double> lnz = new List<double>();
            for (int i = 0; i < n; i++)
            {
                lnx.Add(Math.Log(x[i]));
                lny.Add(Math.Log(y[i]));
                lnz.Add(Math.Log(z[i]));
            }
            List<double> coef = LinearAprox(lnx, lny, lnz);
            coef[2] = Math.Exp(coef[2]);
            return coef;
        }
    }
}