﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
namespace ExpDataAnalysis
{
    static class Plot
    {
        /// <summary>
        /// Строит график апроксимурующей функции по заданным данным
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="mode">Если true, то квадратичная апроксимация, иначе линейная</param>
        public static (double, List<double>) Plot2D(List<double> x, List<double> y, double x0, int mode = 0)
        {
            MainForm.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            int N = 100;
            string dataFile = "data.txt";                  // one data file
            string gnuplotScript = "gnuplotScript.plt";    // gnuplot script
            string pngFile = "trajectory.png";             // output png file
            double xmin = Math.Min(x.Min(), x0);
            double xmax = Math.Max(x.Max(), x0);
            // init values
            double[] xl = new double[N + 1];
            double[] yl = new double[N + 1];
            for (int i = 0; i <= N; i++)
            {
                xl[i] = xmin + (xmax - xmin) * i / N;
            }
            double y0 = 0;
            // US output standard
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
            List<double> coef = new List<double>();
            switch (mode)
            {
                case 0:
                    coef = OneDimAprox.LinearAprox(x, y);
                    for (int i = 0; i <= N; i++)
                    {
                        yl[i] = coef[0] * xl[i] + coef[1];
                    }
                    y0 = coef[0] * x0 + coef[1];
                    break;
                case 1:
                    coef = OneDimAprox.QuadraticAprox(x, y);
                    for (int i = 0; i <= N; i++)
                    {
                        yl[i] = coef[0] * Math.Pow(xl[i], 2) + coef[1] * xl[i] + coef[2];
                    }
                    y0 = coef[0] * Math.Pow(x0, 2) + coef[1] * x0 + coef[2];
                    break;
                case 2:
                    coef = OneDimAprox.DegreeAprox(x, y);
                    for (int i = 0; i <= N; i++)
                    {
                        yl[i] = coef[1] * Math.Pow(xl[i], coef[0]);
                    }
                    y0 = coef[1] * Math.Pow(x0, coef[0]);
                    break;
            }
            // generate data for visualisation
            using (StreamWriter sw = new StreamWriter(dataFile))
            {
                for (int i = 0; i <= N; i++)
                {
                    sw.WriteLine(xl[i].ToString("F8") + "\t" + yl[i].ToString("F8"));
                }
            }

            string gnuplot_script = "set encoding utf8\n" +
                                    "set title \"Approximation\"\n" +
                                    "set xlabel \"x\"\n" +
                                    "set ylabel \"y\"\n" +
                                    "set term pngcairo size 1920,1080 font \"Arial,18\"\n" +
                                    "set output \"pngFile\"\n" +
                                    "plot 'dataFile' with lines title \"Approximation\"\n" +
                                    "end";
            // change filenames in script
            gnuplot_script = gnuplot_script.Replace("dataFile", dataFile);
            gnuplot_script = gnuplot_script.Replace("pngFile", pngFile);
            // write script to file
            using (StreamWriter sw = new StreamWriter(gnuplotScript))
            {
                sw.WriteLine(gnuplot_script);
            }
            // launch script
            ProcessStartInfo PSI = new ProcessStartInfo
            {
                FileName = gnuplotScript
            };
            string dir = Directory.GetCurrentDirectory();
            PSI.WorkingDirectory = dir;
            using (Process exeProcess = Process.Start(PSI))
            {
                exeProcess.WaitForExit();
            }
            MainForm.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            using (FileStream fs = new FileStream(pngFile, FileMode.Open))
            {
                MainForm.pictureBox.Image = Image.FromStream(fs);
            }
            return (y0, coef);
        }
        public static (double, List<double>) Plot3D(List<double> x, List<double> y, List<double> z, double x0, double y0, int mode = 0)
        {
            MainForm.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            int N = 20;
            string dataFile = "data.txt";                  // one data file
            string gnuplotScript = "gnuplotScript.plt";    // gnuplot script
            string pngFile = "trajectory.png";             // output png file
            double xmin = Math.Min(x.Min(), x0);
            double xmax = Math.Max(x.Max(), x0);
            double ymin = Math.Min(y.Min(), y0);
            double ymax = Math.Max(y.Max(), y0);
            // init values
            double[] xl = new double[N + 1];
            double[] yl = new double[N + 1];
            for (int i = 0; i <= N; i++)
            {
                xl[i] = xmin + (xmax - xmin) * i / N;
                yl[i] = ymin + (ymax - ymin) * i / N;
            }
            StreamWriter sw = new StreamWriter(dataFile);

            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");

            List<double> coef = new List<double>();
            double z0 = 0;
            sw.Write(N + 1);
            for (int i = 0; i <= N; i++)
            {
                sw.Write("\t" + xl[i].ToString("F8"));
            }
            switch (mode)
            {
                case 0:
                    coef = TwoDimAprox.LinearAprox(x, y, z);
                    for (int i = 0; i <= N; i++)
                    {
                        sw.Write("\n" + yl[i].ToString("F8"));
                        for (int j = 0; j <= N; j++)
                        {
                            sw.Write("\t" + (coef[0] * xl[j] + coef[1] * yl[i] + coef[2]).ToString("F8"));
                        }
                    }
                    z0 = coef[0] * x0 + coef[1] * y0 + coef[2];
                    break;
                case 1:
                    coef = TwoDimAprox.QuadraticAprox(x, y, z);
                    for (int i = 0; i <= N; i++)
                    {
                        sw.Write("\n" + yl[i].ToString("F8"));
                        for (int j = 0; j <= N; j++)
                        {
                            sw.Write("\t" + (coef[0] * Math.Pow(xl[j], 2) + coef[1] * xl[j] * yl[i] + coef[2] * Math.Pow(yl[i], 2)
                                + coef[3] * xl[j] + coef[4] * yl[i] + coef[5]).ToString("F8"));
                        }
                    }
                    z0 = coef[0] * Math.Pow(x0, 2) + coef[1] * x0 * y0 + coef[2] * Math.Pow(y0, 2)
                                 + coef[3] * x0 + coef[4] * y0 + coef[5];
                    break;
                case 2:
                    coef = TwoDimAprox.DegreeAprox(x, y, z);
                    for (int i = 0; i <= N; i++)
                    {
                        sw.Write("\n" + yl[i].ToString("F8"));
                        for (int j = 0; j <= N; j++)
                        {
                            sw.Write("\t" + (coef[2] * Math.Pow(xl[j], coef[0]) * Math.Pow(yl[i], coef[1])).ToString("F8"));
                        }
                    }
                    z0 = coef[2] * Math.Pow(x0, coef[0]) * Math.Pow(y0, coef[1]);
                    break;
            }
            sw.Close();
            string gnuplot_script = "set view 50,20\n" +
                "set title \"Approximation\"\n" +
                                    "set ticslevel 0\n" +
                                    "set xlabel \"x1\"\n" +
                                    "set ylabel \"x2\"\n" +
                                    "set zlabel \"y\"\n" +
                                    "set hidden3d\n" +
                                    "set term pngcairo size 1920,1080 font \"Arial,18\"\n" +
                                    "set output \"pngFile\"\n" +
                                    "splot 'dataFile' matrix nonuniform with lines t ''";
            // change filenames in script
            gnuplot_script = gnuplot_script.Replace("dataFile", dataFile);
            gnuplot_script = gnuplot_script.Replace("pngFile", pngFile);
            // write script to file
            sw = new StreamWriter(gnuplotScript, false, new System.Text.UTF8Encoding(false));
            sw.WriteLine(gnuplot_script);
            sw.Close();
            // launch script
            ProcessStartInfo PSI = new ProcessStartInfo
            {
                FileName = gnuplotScript
            };
            string dir = Directory.GetCurrentDirectory();
            PSI.WorkingDirectory = dir;
            using (Process exeProcess = Process.Start(PSI))
            {
                exeProcess.WaitForExit();
            }
            MainForm.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            using (FileStream fs = new FileStream(pngFile, FileMode.Open))
            {
                MainForm.pictureBox.Image = Image.FromStream(fs);
            }
            return (z0, coef);
        }
    }
}