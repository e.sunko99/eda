﻿using System;
public class Matrix
{
    private readonly double[,] data;
    private double precalculatedDeterminant = double.NaN;
    public int M { get; }
    public int N { get; }
    public bool IsSquare { get => M == N; }
    public void ProcessFunctionOverData(Action<int, int> func)
    {
        for (var i = 0; i < M; i++)
        {
            for (var j = 0; j < N; j++)
            {
                func(i, j);
            }
        }
    }
    public Matrix(int m, int n)
    {
        M = m;
        N = n;
        data = new double[m, n];
        ProcessFunctionOverData((i, j) => data[i, j] = 0);
    }
    public void Set(double[,] input)
    {
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                data[i, j] = input[i, j];
        precalculatedDeterminant = double.NaN;
    }
    public double this[int x, int y]
    {
        get
        {
            return data[x, y];
        }
        set
        {
            data[x, y] = value;
            precalculatedDeterminant = double.NaN;
        }
    }
    public double CalculateDeterminant()
    {
        if (!double.IsNaN(precalculatedDeterminant))
        {
            return precalculatedDeterminant;
        }
        if (!IsSquare)
        {
            throw new InvalidOperationException("determinant can be calculated only for square matrix");
        }
        if (N == 2)
        {
            return this[0, 0] * this[1, 1] - this[0, 1] * this[1, 0];
        }
        double result = 0;
        for (var j = 0; j < N; j++)
        {
            result += (j % 2 == 1 ? 1 : -1) * this[1, j] *
                CreateMatrixWithoutColumn(j).CreateMatrixWithoutRow(1).CalculateDeterminant();
        }
        precalculatedDeterminant = result;
        return result;
    }
    public Matrix SwapColumns(int i, int j)
    {
        Matrix res = new Matrix(M, N);
        res.Set(data);
        for (int k = 0; k < M; k++)
        {
            (res[k, i], res[k, j]) = (res[k, j], res[k, i]);
        }
        return res;
    }
    public Matrix CreateMatrixWithoutRow(int row)
    {
        if (row < 0 || row >= M)
        {
            throw new ArgumentException("invalid row index");
        }
        Matrix result = new Matrix(M - 1, N);
        result.ProcessFunctionOverData((i, j) => result[i, j] = i < row ? this[i, j] : this[i + 1, j]);
        return result;
    }
    public Matrix CreateMatrixWithoutColumn(int column)
    {
        if (column < 0 || column >= N)
        {
            throw new ArgumentException("invalid column index");
        }
        Matrix result = new Matrix(M, N - 1);
        result.ProcessFunctionOverData((i, j) => result[i, j] = j < column ? this[i, j] : this[i, j + 1]);
        return result;
    }
}