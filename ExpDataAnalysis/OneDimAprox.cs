﻿using System;
using System.Collections.Generic;

namespace ExpDataAnalysis
{
    static class OneDimAprox
    {
        public static List<double> LinearAprox(List<double> x, List<double> y)
        {
            double sumx = 0;
            double sumy = 0;
            double sumx2 = 0;
            double sumxy = 0;
            double n = x.Count;
            for (int i = 0; i < n; i++)
            {
                sumx += x[i];
                sumy += y[i];
                sumx2 += Math.Pow(x[i], 2);
                sumxy += x[i] * y[i];
            }
            double a = (n * sumxy - sumx * sumy) / (n * sumx2 - Math.Pow(sumx, 2));
            double b = (sumy - a * sumx) / n;
            return new List<double> { a, b };
        }

        public static List<double> QuadraticAprox(List<double> x, List<double> y)
        {
            double sumx4 = 0;
            double sumx3 = 0;
            double sumx2 = 0;
            double sumx = 0;
            double sumx2y = 0;
            double sumxy = 0;
            double sumy = 0;
            double n = x.Count;
            for (int i = 0; i < n; i++)
            {
                sumx4 += Math.Pow(x[i], 4);
                sumx3 += Math.Pow(x[i], 3);
                sumx2 += Math.Pow(x[i], 2);
                sumx += x[i];
                sumx2y += Math.Pow(x[i], 2) * y[i];
                sumxy += x[i] * y[i];
                sumy += y[i];
            }
            double d = sumx4 * (sumx2 * n - sumx * sumx) - sumx3 * (sumx3 * n - sumx * sumx2) + sumx2 * (sumx3 * sumx - sumx2 * sumx2);
            double d1 = sumx2y * (sumx2 * n - sumx * sumx) - sumx3 * (sumxy * n - sumx * sumy) + sumx2 * (sumxy * sumx - sumy * sumx2);
            double d2 = sumx4 * (sumxy * n - sumy * sumx) - sumx2y * (sumx3 * n - sumx * sumx2) + sumx2 * (sumx3 * sumy - sumxy * sumx2);
            double d3 = sumx4 * (sumx2 * sumy - sumxy * sumx) - sumx3 * (sumx3 * sumy - sumxy * sumx2) + sumx2y * (sumx3 * sumx - sumx2 * sumx2);
            return new List<double> { d1 / d, d2 / d, d3 / d };
        }

        public static List<double> DegreeAprox(List<double> x, List<double> y)
        {
            int n = x.Count;
            List<double> lnx = new List<double>();
            List<double> lny = new List<double>();
            for (int i = 0; i < n; i++)
            {
                lnx.Add(Math.Log(x[i]));
                lny.Add(Math.Log(y[i]));
            }
            List<double> coef = LinearAprox(lnx, lny);
            coef[1] = Math.Exp(coef[1]);
            return coef;
        }
    }
}