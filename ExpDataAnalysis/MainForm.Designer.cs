﻿
namespace ExpDataAnalysis
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlottingSpace = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX01 = new System.Windows.Forms.Label();
            this.labelX02 = new System.Windows.Forms.Label();
            this.labelY0 = new System.Windows.Forms.Label();
            this.inputX01 = new System.Windows.Forms.NumericUpDown();
            this.inputX02 = new System.Windows.Forms.NumericUpDown();
            this.inputY0 = new System.Windows.Forms.NumericUpDown();
            this.TwoDimMode = new System.Windows.Forms.RadioButton();
            this.OneDimMode = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX1 = new System.Windows.Forms.Label();
            this.labelX2 = new System.Windows.Forms.Label();
            this.labelY = new System.Windows.Forms.Label();
            this.inputX1 = new System.Windows.Forms.NumericUpDown();
            this.inputX2 = new System.Windows.Forms.NumericUpDown();
            this.inputY = new System.Windows.Forms.NumericUpDown();
            this.deletePoint = new System.Windows.Forms.Button();
            this.AddPoint = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.MinLabel = new System.Windows.Forms.Label();
            this.MaxLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.XMin = new System.Windows.Forms.NumericUpDown();
            this.Xsteps = new System.Windows.Forms.NumericUpDown();
            this.Ysteps = new System.Windows.Forms.NumericUpDown();
            this.YMin = new System.Windows.Forms.NumericUpDown();
            this.XMax = new System.Windows.Forms.NumericUpDown();
            this.YMax = new System.Windows.Forms.NumericUpDown();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загрузитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.PlottingSpace)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputX01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputX02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputY0)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputX2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Xsteps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ysteps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PlottingSpace
            // 
            this.PlottingSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PlottingSpace.Location = new System.Drawing.Point(253, 3);
            this.PlottingSpace.Name = "PlottingSpace";
            this.PlottingSpace.Size = new System.Drawing.Size(525, 646);
            this.PlottingSpace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PlottingSpace.TabIndex = 1;
            this.PlottingSpace.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel1.Controls.Add(this.PlottingSpace, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 23);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1100, 632);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.TwoDimMode, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.OneDimMode, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.comboBox1, 0, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(104, 646);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.98058F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.01942F));
            this.tableLayoutPanel5.Controls.Add(this.labelX01, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.labelX02, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.labelY0, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.inputX01, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.inputX02, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.inputY0, 1, 2);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 202);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(103, 83);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // labelX01
            // 
            this.labelX01.AutoSize = true;
            this.labelX01.Location = new System.Drawing.Point(3, 0);
            this.labelX01.Name = "labelX01";
            this.labelX01.Size = new System.Drawing.Size(18, 13);
            this.labelX01.TabIndex = 0;
            this.labelX01.Text = "x1";
            // 
            // labelX02
            // 
            this.labelX02.AutoSize = true;
            this.labelX02.Location = new System.Drawing.Point(3, 28);
            this.labelX02.Name = "labelX02";
            this.labelX02.Size = new System.Drawing.Size(18, 13);
            this.labelX02.TabIndex = 1;
            this.labelX02.Text = "x2";
            // 
            // labelY0
            // 
            this.labelY0.AutoSize = true;
            this.labelY0.Location = new System.Drawing.Point(3, 56);
            this.labelY0.Name = "labelY0";
            this.labelY0.Size = new System.Drawing.Size(12, 13);
            this.labelY0.TabIndex = 2;
            this.labelY0.Text = "y";
            // 
            // inputX01
            // 
            this.inputX01.DecimalPlaces = 3;
            this.inputX01.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.inputX01.Location = new System.Drawing.Point(37, 3);
            this.inputX01.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.inputX01.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.inputX01.Name = "inputX01";
            this.inputX01.Size = new System.Drawing.Size(62, 20);
            this.inputX01.TabIndex = 3;
            this.inputX01.ValueChanged += new System.EventHandler(this.InputX01_ValueChanged);
            // 
            // inputX02
            // 
            this.inputX02.DecimalPlaces = 3;
            this.inputX02.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.inputX02.Location = new System.Drawing.Point(37, 31);
            this.inputX02.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.inputX02.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.inputX02.Name = "inputX02";
            this.inputX02.Size = new System.Drawing.Size(62, 20);
            this.inputX02.TabIndex = 4;
            this.inputX02.ValueChanged += new System.EventHandler(this.InputX02_ValueChanged);
            // 
            // inputY0
            // 
            this.inputY0.DecimalPlaces = 3;
            this.inputY0.Enabled = false;
            this.inputY0.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.inputY0.Location = new System.Drawing.Point(37, 59);
            this.inputY0.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.inputY0.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.inputY0.Name = "inputY0";
            this.inputY0.Size = new System.Drawing.Size(62, 20);
            this.inputY0.TabIndex = 5;
            // 
            // TwoDimMode
            // 
            this.TwoDimMode.AutoSize = true;
            this.TwoDimMode.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.TwoDimMode.Location = new System.Drawing.Point(3, 39);
            this.TwoDimMode.Name = "TwoDimMode";
            this.TwoDimMode.Size = new System.Drawing.Size(99, 30);
            this.TwoDimMode.TabIndex = 1;
            this.TwoDimMode.Text = "Двумерная\r\nапроксимация";
            this.TwoDimMode.UseVisualStyleBackColor = true;
            // 
            // OneDimMode
            // 
            this.OneDimMode.AutoSize = true;
            this.OneDimMode.Checked = true;
            this.OneDimMode.Location = new System.Drawing.Point(3, 3);
            this.OneDimMode.Name = "OneDimMode";
            this.OneDimMode.Size = new System.Drawing.Size(99, 30);
            this.OneDimMode.TabIndex = 0;
            this.OneDimMode.TabStop = true;
            this.OneDimMode.Text = "Одномерная\r\nапроксимация";
            this.OneDimMode.UseVisualStyleBackColor = true;
            this.OneDimMode.CheckedChanged += new System.EventHandler(this.ChangeDimension);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.98058F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.01942F));
            this.tableLayoutPanel3.Controls.Add(this.labelX1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelX2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.labelY, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.inputX1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.inputX2, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.inputY, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.deletePoint, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.AddPoint, 0, 3);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 75);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(103, 121);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // labelX1
            // 
            this.labelX1.AutoSize = true;
            this.labelX1.Location = new System.Drawing.Point(3, 0);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(12, 13);
            this.labelX1.TabIndex = 0;
            this.labelX1.Text = "x";
            // 
            // labelX2
            // 
            this.labelX2.AutoSize = true;
            this.labelX2.Location = new System.Drawing.Point(3, 26);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(18, 13);
            this.labelX2.TabIndex = 1;
            this.labelX2.Text = "x2";
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(3, 52);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(12, 13);
            this.labelY.TabIndex = 2;
            this.labelY.Text = "y";
            // 
            // inputX1
            // 
            this.inputX1.DecimalPlaces = 1;
            this.inputX1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.inputX1.Location = new System.Drawing.Point(37, 3);
            this.inputX1.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.inputX1.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.inputX1.Name = "inputX1";
            this.inputX1.Size = new System.Drawing.Size(62, 20);
            this.inputX1.TabIndex = 3;
            // 
            // inputX2
            // 
            this.inputX2.DecimalPlaces = 1;
            this.inputX2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.inputX2.Location = new System.Drawing.Point(37, 29);
            this.inputX2.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.inputX2.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.inputX2.Name = "inputX2";
            this.inputX2.Size = new System.Drawing.Size(62, 20);
            this.inputX2.TabIndex = 4;
            // 
            // inputY
            // 
            this.inputY.DecimalPlaces = 1;
            this.inputY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.inputY.Location = new System.Drawing.Point(37, 55);
            this.inputY.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.inputY.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.inputY.Name = "inputY";
            this.inputY.Size = new System.Drawing.Size(62, 20);
            this.inputY.TabIndex = 5;
            // 
            // deletePoint
            // 
            this.deletePoint.Location = new System.Drawing.Point(37, 81);
            this.deletePoint.Name = "deletePoint";
            this.deletePoint.Size = new System.Drawing.Size(62, 37);
            this.deletePoint.TabIndex = 6;
            this.deletePoint.Text = "Удалить";
            this.deletePoint.UseVisualStyleBackColor = true;
            this.deletePoint.Click += new System.EventHandler(this.DeletePoint_Click);
            // 
            // AddPoint
            // 
            this.AddPoint.Location = new System.Drawing.Point(3, 81);
            this.AddPoint.Name = "AddPoint";
            this.AddPoint.Size = new System.Drawing.Size(28, 37);
            this.AddPoint.TabIndex = 7;
            this.AddPoint.Text = "+";
            this.AddPoint.UseVisualStyleBackColor = true;
            this.AddPoint.Click += new System.EventHandler(this.AddPoint_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Линейная",
            "Квадратичная ",
            "Степенная"});
            this.comboBox1.Location = new System.Drawing.Point(3, 291);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(103, 21);
            this.comboBox1.TabIndex = 5;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ChangeDegree);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView.Location = new System.Drawing.Point(113, 3);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowTemplate.ReadOnly = true;
            this.dataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView.Size = new System.Drawing.Size(134, 646);
            this.dataGridView.TabIndex = 3;
            this.dataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_CellClick);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column1.HeaderText = "X1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 26;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Column2.HeaderText = "X2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 26;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.HeaderText = "Y";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.dataGridView1, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(784, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.76471F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.23529F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(313, 646);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.Controls.Add(this.MinLabel, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.MaxLabel, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.label1, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.XMin, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.Xsteps, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.Ysteps, 3, 2);
            this.tableLayoutPanel7.Controls.Add(this.YMin, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.XMax, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.YMax, 2, 2);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(307, 70);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // MinLabel
            // 
            this.MinLabel.AutoSize = true;
            this.MinLabel.Location = new System.Drawing.Point(79, 0);
            this.MinLabel.Name = "MinLabel";
            this.MinLabel.Size = new System.Drawing.Size(23, 13);
            this.MinLabel.TabIndex = 0;
            this.MinLabel.Text = "min";
            // 
            // MaxLabel
            // 
            this.MaxLabel.AutoSize = true;
            this.MaxLabel.Location = new System.Drawing.Point(155, 0);
            this.MaxLabel.Name = "MaxLabel";
            this.MaxLabel.Size = new System.Drawing.Size(26, 13);
            this.MaxLabel.TabIndex = 1;
            this.MaxLabel.Text = "max";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(231, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "steps";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "x1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "x2";
            // 
            // XMin
            // 
            this.XMin.DecimalPlaces = 3;
            this.XMin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.XMin.Location = new System.Drawing.Point(79, 26);
            this.XMin.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.XMin.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.XMin.Name = "XMin";
            this.XMin.Size = new System.Drawing.Size(68, 20);
            this.XMin.TabIndex = 5;
            this.XMin.ValueChanged += new System.EventHandler(this.XMin_ValueChanged);
            // 
            // Xsteps
            // 
            this.Xsteps.Location = new System.Drawing.Point(231, 26);
            this.Xsteps.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Xsteps.Name = "Xsteps";
            this.Xsteps.Size = new System.Drawing.Size(68, 20);
            this.Xsteps.TabIndex = 7;
            this.Xsteps.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Xsteps.ValueChanged += new System.EventHandler(this.XMin_ValueChanged);
            // 
            // Ysteps
            // 
            this.Ysteps.Location = new System.Drawing.Point(231, 49);
            this.Ysteps.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Ysteps.Name = "Ysteps";
            this.Ysteps.Size = new System.Drawing.Size(68, 20);
            this.Ysteps.TabIndex = 8;
            this.Ysteps.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Ysteps.ValueChanged += new System.EventHandler(this.XMin_ValueChanged);
            // 
            // YMin
            // 
            this.YMin.DecimalPlaces = 3;
            this.YMin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.YMin.Location = new System.Drawing.Point(79, 49);
            this.YMin.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.YMin.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.YMin.Name = "YMin";
            this.YMin.Size = new System.Drawing.Size(68, 20);
            this.YMin.TabIndex = 9;
            this.YMin.ValueChanged += new System.EventHandler(this.XMin_ValueChanged);
            // 
            // XMax
            // 
            this.XMax.DecimalPlaces = 3;
            this.XMax.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.XMax.Location = new System.Drawing.Point(155, 26);
            this.XMax.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.XMax.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.XMax.Name = "XMax";
            this.XMax.Size = new System.Drawing.Size(68, 20);
            this.XMax.TabIndex = 10;
            this.XMax.ValueChanged += new System.EventHandler(this.XMin_ValueChanged);
            // 
            // YMax
            // 
            this.YMax.DecimalPlaces = 3;
            this.YMax.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.YMax.Location = new System.Drawing.Point(155, 49);
            this.YMax.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.YMax.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.YMax.Name = "YMax";
            this.YMax.Size = new System.Drawing.Size(68, 20);
            this.YMax.TabIndex = 11;
            this.YMax.ValueChanged += new System.EventHandler(this.XMin_ValueChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ColumnHeadersVisible = false;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 79);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(307, 564);
            this.dataGridView1.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.menuStrip1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.statusStrip1, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1106, 680);
            this.tableLayoutPanel4.TabIndex = 4;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1106, 20);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.загрузитьToolStripMenuItem,
            this.сохранитьToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 16);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // загрузитьToolStripMenuItem
            // 
            this.загрузитьToolStripMenuItem.Name = "загрузитьToolStripMenuItem";
            this.загрузитьToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.загрузитьToolStripMenuItem.Text = "Загрузить";
            this.загрузитьToolStripMenuItem.Click += new System.EventHandler(this.ЗагрузитьToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.СохранитьToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 658);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(135, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1106, 680);
            this.Controls.Add(this.tableLayoutPanel4);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Анализ Данных Эксперимента";
            ((System.ComponentModel.ISupportInitialize)(this.PlottingSpace)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputX01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputX02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputY0)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.inputX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputX2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Xsteps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ysteps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox PlottingSpace;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RadioButton TwoDimMode;
        private System.Windows.Forms.RadioButton OneDimMode;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label labelX1;
        private System.Windows.Forms.Label labelX2;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.NumericUpDown inputX1;
        private System.Windows.Forms.NumericUpDown inputX2;
        private System.Windows.Forms.NumericUpDown inputY;
        private System.Windows.Forms.Button deletePoint;
        private System.Windows.Forms.Button AddPoint;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label labelX01;
        private System.Windows.Forms.Label labelX02;
        private System.Windows.Forms.Label labelY0;
        private System.Windows.Forms.NumericUpDown inputX01;
        private System.Windows.Forms.NumericUpDown inputX02;
        private System.Windows.Forms.NumericUpDown inputY0;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem загрузитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label MinLabel;
        private System.Windows.Forms.Label MaxLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown XMin;
        private System.Windows.Forms.NumericUpDown Xsteps;
        private System.Windows.Forms.NumericUpDown Ysteps;
        private System.Windows.Forms.NumericUpDown YMin;
        private System.Windows.Forms.NumericUpDown XMax;
        private System.Windows.Forms.NumericUpDown YMax;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}

