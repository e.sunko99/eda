﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
namespace ExpDataAnalysis
{
    public partial class MainForm : Form
    {
        public static PictureBox pictureBox;
        private readonly List<double> y = new List<double>();
        private readonly List<double> x1 = new List<double>();
        private readonly List<double> x2 = new List<double>();
        int mode;
        List<double> coef;
        private void AddPoint_Click(object sender, EventArgs e)
        {
            x1.Add(Convert.ToDouble(inputX1.Value));
            x2.Add(Convert.ToDouble(inputX2.Value));
            y.Add(Convert.ToDouble(inputY.Value));
            dataGridView.Rows.Add(x1.Last(), x2.Last(), y.Last());
            StartPlot();
        }
        public MainForm()
        {
            InitializeComponent();
            OneDimMode.Checked = false;
            OneDimMode.Checked = true;
            pictureBox = PlottingSpace;
            StartPlot();
        }
        private void DeletePoint_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewCell cell in dataGridView.SelectedCells)
            {
                DataGridViewRow drv = cell.OwningRow;
                int i = drv.Index;
                if (i != -1)
                {
                    x1.RemoveAt(i);
                    x2.RemoveAt(i);
                    y.RemoveAt(i);
                    dataGridView.Rows.Remove(drv);
                }
            }
            if (y.Count > 1)
                StartPlot();
        }
        private void ChangeDegree(object sender, EventArgs e)
        {
            mode = comboBox1.SelectedIndex;
            if (x1.Count > 1)
                StartPlot();
        }
        private void ChangeDimension(object sender, EventArgs e)
        {
            x1.Clear();
            x2.Clear();
            y.Clear();
            dataGridView.Columns[1].Visible = !OneDimMode.Checked;
            inputX2.Visible = !OneDimMode.Checked;
            inputX02.Visible = !OneDimMode.Checked;
            dataGridView.Rows.Clear();
            inputX2.Value = 0;
            inputX02.Value = 0;
            labelX2.Visible = !OneDimMode.Checked;
            labelX02.Visible = !OneDimMode.Checked;
            if (OneDimMode.Checked)
            {
                dataGridView.Columns[0].HeaderText = "X";
                labelX1.Text = "x";
            }
            else
            {
                dataGridView.Columns[0].HeaderText = "X1";
                labelX1.Text = "x1";

            }
            labelX01.Text = labelX1.Text;
        }
        private void StartPlot()
        {
            double x;
            try
            {
                double xMin, xMax, yMin, yMax;
                int nx, ny;
                xMin = Convert.ToDouble(XMin.Value);
                xMax = Convert.ToDouble(XMax.Value);
                yMin = Convert.ToDouble(YMin.Value);
                yMax = Convert.ToDouble(YMax.Value);
                nx = Convert.ToInt32(Xsteps.Value);
                ny = Convert.ToInt32(Ysteps.Value);
                dataGridView1.RowCount = nx + 2;
                dataGridView1.ColumnCount = ny + 2;
                foreach (DataGridViewColumn a in dataGridView1.Columns)
                    a.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                if (OneDimMode.Checked)
                {
                    dataGridView1.ColumnCount = 2;
                    (x, coef) = Plot.Plot2D(x1, y, Convert.ToDouble(inputX01.Value), mode);
                    switch (mode)
                    {
                        case 0:

                            toolStripStatusLabel1.Text = string.Format("y={0:0.00000}*x+{1:0.00000}", coef[0], coef[1]);
                            for (int i = 0; i <= nx; i++)
                            {
                                double xi = xMin + (xMax - xMin) * i / nx;
                                dataGridView1.Rows[i].Cells[0].Value = xi.ToString("F4");
                                dataGridView1.Rows[i].Cells[1].Value = (coef[0] * xi + coef[1]).ToString("F4");
                            }
                            break;
                        case 1:

                            toolStripStatusLabel1.Text = string.Format("y={0:0.00000}*x^2+{1:0.00000}*x+{2:0.00000}", coef[0], coef[1], coef[2]);
                            for (int i = 0; i <= nx; i++)
                            {
                                double xi = xMin + (xMax - xMin) * i / nx;
                                dataGridView1.Rows[i].Cells[0].Value = xi.ToString("F5");
                                dataGridView1.Rows[i].Cells[1].Value = (coef[0] * Math.Pow(xi, 2) + coef[1] * xi + coef[2]).ToString("F4");
                            }
                            break;
                        case 2:
                            toolStripStatusLabel1.Text = string.Format("y={0:0.00000}*x^{1:0.00000}", coef[1], coef[0]);
                            for (int i = 0; i <= nx; i++)
                            {
                                double xi = xMin + (xMax - xMin) * i / nx;
                                dataGridView1.Rows[i].Cells[0].Value = xi.ToString("F5");
                                dataGridView1.Rows[i].Cells[1].Value = (coef[1] * Math.Pow(xi, coef[0])).ToString("F4");
                            }
                            break;
                    }
                }
                else
                {
                    dataGridView1.Rows[0].Cells[0].Value = "x|y";
                    (x, coef) = Plot.Plot3D(x1, x2, y, Convert.ToDouble(inputX01.Value), Convert.ToDouble(inputX02.Value), mode);
                    for (int i = 0; i <= ny; i++)
                        dataGridView1.Rows[0].Cells[i + 1].Value = (yMin + (yMax - yMin) * i / ny).ToString("F4");
                    double xi;
                    double yj;
                    switch (mode)
                    {
                        case 0:
                            toolStripStatusLabel1.Text = string.Format("z={0:0.00000}*x+{1:0.00000}*y+{2:0.00000}", coef[0], coef[1], coef[2]);
                            for (int i = 0; i <= nx; i++)
                            {
                                xi = xMin + (xMax - xMin) * i / nx;
                                dataGridView1.Rows[i + 1].Cells[0].Value = xi.ToString("F4");
                                for (int j = 0; j <= ny; j++)
                                {
                                    yj = (yMin + (yMax - yMin) * j / ny);
                                    dataGridView1.Rows[i + 1].Cells[j + 1].Value = (coef[0] * xi + coef[1] * yj + coef[2]).ToString("F4");
                                }

                            }
                            break;
                        case 1:
                            toolStripStatusLabel1.Text = string.Format("z={0:0.00000}*x^2+{1:0.00000}*xy+{2:0.00000}*y^2" +
                                "+{3:0.00000}*x+{4:0.00000}*y+{5:0.00000}", coef[0], coef[1], coef[2], coef[3], coef[4], coef[5]);
                            for (int i = 0; i <= nx; i++)
                            {
                                xi = xMin + (xMax - xMin) * i / nx;
                                dataGridView1.Rows[i + 1].Cells[0].Value = xi.ToString("F4");
                                for (int j = 0; j <= ny; j++)
                                {
                                    yj = (yMin + (yMax - yMin) * j / ny);
                                    dataGridView1.Rows[i + 1].Cells[j + 1].Value = (coef[0] * Math.Pow(xi, 2) + coef[1] * xi * yj + coef[2] * Math.Pow(yj, 2) +
                                        coef[3] * xi + coef[4] * yj + coef[5]).ToString("F4");
                                }
                            }
                            break;
                        case 2:
                            toolStripStatusLabel1.Text = string.Format("z={0:0.00000}*x^{1:0.00000}*y^{2:0.00000}", coef[2], coef[0], coef[1]);
                            for (int i = 0; i <= nx; i++)
                            {
                                xi = xMin + (xMax - xMin) * i / nx;
                                dataGridView1.Rows[i + 1].Cells[0].Value = xi.ToString("F4");
                                for (int j = 0; j <= ny; j++)
                                {
                                    yj = (yMin + (yMax - yMin) * j / ny);
                                    dataGridView1.Rows[i + 1].Cells[j + 1].Value = (coef[2] * Math.Pow(xi, coef[0]) * Math.Pow(yj, coef[1])).ToString("F4");
                                }

                            }
                            break;
                    }
                }
                inputY0.Value = Convert.ToDecimal(x);
            }
            catch
            {
            }
        }
        private void DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int i = e.RowIndex;
            if (i < 0)
                return;
            inputX1.Value = Convert.ToDecimal(dataGridView.Rows[i].Cells[0].Value);
            inputX2.Value = Convert.ToDecimal(dataGridView.Rows[i].Cells[1].Value);
            inputY.Value = Convert.ToDecimal(dataGridView.Rows[i].Cells[2].Value);
        }
        private void InputX01_ValueChanged(object sender, EventArgs e)
        {
            StartPlot();
        }
        private void InputX02_ValueChanged(object sender, EventArgs e)
        {
            StartPlot();
        }
        private void ЗагрузитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                dataGridView.Rows.Clear();
                x1.Clear();
                x2.Clear();
                y.Clear();
                using (StreamReader sr = new StreamReader(OFD.FileName))
                {
                    int dim = 1;
                    if (sr.ReadLine() == "1")
                    {
                        OneDimMode.Checked = true;
                    }
                    else
                    {
                        TwoDimMode.Checked = true;
                        dim = 2;
                    }
                    while (!sr.EndOfStream)
                    {
                        string[] point = sr.ReadLine().Replace(',', '.').Split(' ');
                        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                        x1.Add(double.Parse(point[0]));
                        x2.Add(double.Parse(point[1]));
                        if (dim == 1)
                        {
                            y.Add(double.Parse(point[1]));
                        }
                        else
                        {
                            y.Add(double.Parse(point[2]));
                        }
                        dataGridView.Rows.Add(x1.Last(), x2.Last(), y.Last());
                    }
                }
                StartPlot();
            }
        }
        private void СохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog SFD = new SaveFileDialog
            {
                Filter = "txt files(*.txt)| *.txt | All files(*.*) | *.* ",
                FilterIndex = 2,
                RestoreDirectory = true
            };
            int dim = 1;
            if (SFD.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(SFD.FileName))
                {
                    if (OneDimMode.Checked)
                    {
                        sw.WriteLine(1);
                    }
                    else
                    {
                        dim = 2;
                        sw.WriteLine(2);
                    }
                    for (int i = 0; i < x1.Count; i++)
                    {
                        sw.Write(String.Format("{0:0.00000} ", x1[i]));
                        if (dim == 2)
                            sw.Write(String.Format("{0:0.00000} ", x2[i]));
                        sw.Write(String.Format("{0:0.00000}\n", y[i]));
                    }
                }
            }
        }
        private void XMin_ValueChanged(object sender, EventArgs e)
        {
            StartPlot();
        }
    }
}